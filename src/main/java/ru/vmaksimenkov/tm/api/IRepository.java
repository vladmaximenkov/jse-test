package ru.vmaksimenkov.tm.api;

import ru.vmaksimenkov.tm.model.AbstractEntity;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

public interface IRepository<E extends AbstractEntity> {

    void add(E entity);

    void clear(String userId);

    boolean existsById(String userId, String id);

    Stream<E> findAll(String userId, Comparator<E> comparator);

    Stream<E> findAll(String userId);

    Optional<E> findById(String userId, String id);

    void remove(E entity);

    void removeById(String userId, String id);

    int size();

    int size(String userId);

}
