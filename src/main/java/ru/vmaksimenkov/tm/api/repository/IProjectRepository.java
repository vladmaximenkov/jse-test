package ru.vmaksimenkov.tm.api.repository;

import ru.vmaksimenkov.tm.api.IRepository;
import ru.vmaksimenkov.tm.model.Project;

import java.util.Optional;

public interface IProjectRepository extends IRepository<Project> {

    boolean existsByName(String userId, String name);

    Optional<Project> findOneByIndex(String userId, Integer index);

    Optional<Project> findOneByName(String userId, String name);

    String getIdByIndex(String userId, Integer index);

    String getIdByName(String userId, String name);

    void removeOneByIndex(String userId, Integer index);

    void removeOneByName(String userId, String name);

}
