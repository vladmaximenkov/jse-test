package ru.vmaksimenkov.tm.api.repository;

import ru.vmaksimenkov.tm.api.IRepository;
import ru.vmaksimenkov.tm.model.Task;

import java.util.Optional;
import java.util.stream.Stream;

public interface ITaskRepository extends IRepository<Task> {

    void bindTaskPyProjectId(String userId, String projectId, String taskId);

    boolean existsByName(String userId, String name);

    boolean existsByProjectId(String userId, String projectId);

    Stream<Task> findAllByProjectId(String userId, String projectId);

    Optional<Task> findOneByIndex(String userId, Integer index);

    Optional<Task> findOneByName(String userId, String name);

    String getIdByIndex(String userId, Integer index);

    void removeAllBinded(String userId);

    void removeAllByProjectId(String userId, String projectId);

    void removeOneByIndex(String userId, Integer index);

    void removeOneByName(String userId, String name);

    Optional<Task> unbindTaskFromProject(String userId, String taskId);

}
