package ru.vmaksimenkov.tm.api.repository;

import ru.vmaksimenkov.tm.api.IRepository;
import ru.vmaksimenkov.tm.model.User;

import java.util.Optional;

public interface IUserRepository extends IRepository<User> {

    boolean existsByEmail(String email);

    boolean existsByLogin(String login);

    Optional<User> findById(String id);

    Optional<User> findByLogin(String login);

    void removeByLogin(String userId, String login);

    void setPasswordById(String userId, String id, String password);

}
