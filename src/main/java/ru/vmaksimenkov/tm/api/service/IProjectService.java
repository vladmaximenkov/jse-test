package ru.vmaksimenkov.tm.api.service;

import ru.vmaksimenkov.tm.api.IService;
import ru.vmaksimenkov.tm.enumerated.Status;
import ru.vmaksimenkov.tm.model.Project;

import java.util.Optional;

public interface IProjectService extends IService<Project> {

    Project add(String userId, String name, String description);

    boolean existsByName(String userId, String name);

    Optional<Project> findOneByIndex(String userId, Integer index);

    Optional<Project> findOneByName(String userId, String name);

    Optional<Project> finishProjectById(String userId, String id);

    Optional<Project> finishProjectByIndex(String userId, Integer index);

    Optional<Project> finishProjectByName(String userId, String name);

    Optional<Project> setProjectStatusById(String userId, String id, Status status);

    Optional<Project> setProjectStatusByIndex(String userId, Integer index, Status status);

    Optional<Project> setProjectStatusByName(String userId, String name, Status status);

    Optional<Project> startProjectById(String userId, String id);

    Optional<Project> startProjectByIndex(String userId, Integer index);

    Optional<Project> startProjectByName(String userId, String name);

    Project updateProjectById(String userId, String id, String name, String description);

    Project updateProjectByIndex(String userId, Integer index, String name, String description);

    Project updateProjectByName(String userId, String name, String nameNew, String description);

}
