package ru.vmaksimenkov.tm.api.service;

import ru.vmaksimenkov.tm.model.Task;

import java.util.Optional;
import java.util.stream.Stream;

public interface IProjectTaskService {

    void bindTaskByProjectId(String userId, String projectId, String taskId);

    void clearTasks(String userId);

    Stream<Task> findAllTaskByProjectId(String userId, String projectId);

    void removeProjectById(String userId, String projectId);

    void removeProjectByIndex(String userId, Integer projectIndex);

    void removeProjectByName(String userId, String projectName);

    Optional<Task> unbindTaskFromProject(String userId, String taskId);

}
