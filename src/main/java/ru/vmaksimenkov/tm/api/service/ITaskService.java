package ru.vmaksimenkov.tm.api.service;

import ru.vmaksimenkov.tm.api.IService;
import ru.vmaksimenkov.tm.enumerated.Status;
import ru.vmaksimenkov.tm.model.Task;

import java.util.Optional;

public interface ITaskService extends IService<Task> {

    Task add(String userId, String name, String description);

    boolean existsByName(String userId, String name);

    Optional<Task> findOneByIndex(String userId, Integer index);

    Optional<Task> findOneByName(String userId, String name);

    Optional<Task> finishTaskById(String userId, String id);

    Optional<Task> finishTaskByIndex(String userId, Integer index);

    Optional<Task> finishTaskByName(String userId, String name);

    String getIdByIndex(String userId, Integer index);

    void removeOneById(String userId, String id);

    void removeOneByIndex(String userId, Integer index);

    void removeOneByName(String userId, String name);

    Optional<Task> setTaskStatusById(String userId, String id, Status status);

    Optional<Task> setTaskStatusByIndex(String userId, Integer index, Status status);

    Optional<Task> setTaskStatusByName(String userId, String name, Status status);

    Optional<Task> startTaskById(String userId, String id);

    Optional<Task> startTaskByIndex(String userId, Integer index);

    Optional<Task> startTaskByName(String userId, String name);

    Task updateTaskById(String userId, String id, String name, String description);

    Task updateTaskByIndex(String userId, Integer index, String name, String description);

    Task updateTaskByName(String userId, String name, String nameNew, String description);

}
