package ru.vmaksimenkov.tm.command.project;

import ru.vmaksimenkov.tm.command.AbstractCommand;
import ru.vmaksimenkov.tm.exception.entity.ProjectNotFoundException;
import ru.vmaksimenkov.tm.model.Project;

import java.util.Optional;

import static ru.vmaksimenkov.tm.util.TerminalUtil.dashedLine;

public abstract class AbstractProjectCommand extends AbstractCommand {

    protected void showProject(final Optional<Project> project) {
        if (!project.isPresent()) throw new ProjectNotFoundException();
        Project project1 = project.get();
        System.out.println("ID: " + project1.getId());
        System.out.println("NAME: " + project1.getName());
        System.out.println("DESCRIPTION: " + project1.getDescription());
        System.out.println("STATUS: " + project1.getStatus().getDisplayName());
        System.out.println("CREATED: " + project1.getCreated());
        System.out.println("STARTED: " + project1.getDateStart());
        System.out.println("FINISHED: " + project1.getDateFinish());
        System.out.print(dashedLine());
    }

}
