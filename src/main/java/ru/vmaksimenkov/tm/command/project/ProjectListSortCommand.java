package ru.vmaksimenkov.tm.command.project;

import ru.vmaksimenkov.tm.enumerated.Sort;
import ru.vmaksimenkov.tm.exception.entity.SortNotFoundException;
import ru.vmaksimenkov.tm.model.Project;
import ru.vmaksimenkov.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Stream;

public final class ProjectListSortCommand extends AbstractProjectCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Show project list sorted";
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[PROJECT LIST SORTED]");
        System.out.println("ENTER SORT:");
        System.out.println(Arrays.toString(Sort.values()));
        Stream<Project> stream;
        try {
            stream = serviceLocator.getProjectService().findAll(userId, Sort.getSort(TerminalUtil.nextLine()).getComparator());
        } catch (SortNotFoundException e) {
            System.err.println(e.getMessage() + " Default sort instead");
            stream = serviceLocator.getProjectService().findAll(userId);
        }
        System.out.printf("\t| %-36s | %-12s | %-20s | %-30s | %-30s | %-30s %n", "ID", "STATUS", "NAME", "CREATED", "STARTED", "FINISHED");
        AtomicInteger index = new AtomicInteger(1);
        stream.forEach((x) -> System.out.println(index.getAndIncrement() + "\t" + x));
    }

    @Override
    public String name() {
        return "project-list-sort";
    }

}
