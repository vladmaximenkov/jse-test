package ru.vmaksimenkov.tm.command.system;

import ru.vmaksimenkov.tm.command.AbstractCommand;

public final class HelpCommand extends AbstractCommand {

    @Override
    public String arg() {
        return "-h";
    }

    @Override
    public String description() {
        return "Show all commands";
    }

    @Override
    public void execute() {
        System.out.println("[HELP]");
        for (final AbstractCommand command : serviceLocator.getCommandService().getCommands()) {
            System.out.println(command.name() + ": " + command.description());
        }
    }

    @Override
    public String name() {
        return "help";
    }
}