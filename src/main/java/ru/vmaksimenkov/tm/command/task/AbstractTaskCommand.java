package ru.vmaksimenkov.tm.command.task;

import ru.vmaksimenkov.tm.command.AbstractCommand;
import ru.vmaksimenkov.tm.exception.entity.TaskNotFoundException;
import ru.vmaksimenkov.tm.model.Task;

import java.util.Optional;

import static ru.vmaksimenkov.tm.util.TerminalUtil.dashedLine;

public abstract class AbstractTaskCommand extends AbstractCommand {

    protected void showTask(final Optional<Task> task) {
        if (!task.isPresent()) throw new TaskNotFoundException();
        Task task1 = task.get();
        System.out.println("ID: " + task1.getId());
        System.out.println("NAME: " + task1.getName());
        System.out.println("DESCRIPTION: " + task1.getDescription());
        System.out.println("STATUS: " + task1.getStatus().getDisplayName());
        System.out.println("PROJECT ID: " + task1.getProjectId());
        System.out.println("CREATED: " + task1.getCreated());
        System.out.println("STARTED: " + task1.getDateStart());
        System.out.println("FINISHED: " + task1.getDateFinish());
        System.out.print(dashedLine());
    }

}
