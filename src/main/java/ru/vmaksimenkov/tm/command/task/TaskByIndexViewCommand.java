package ru.vmaksimenkov.tm.command.task;

import ru.vmaksimenkov.tm.util.TerminalUtil;

public final class TaskByIndexViewCommand extends AbstractTaskCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "View task by index";
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[SHOW TASK]");
        System.out.println("ENTER INDEX:");
        showTask(serviceLocator.getTaskService().findOneByIndex(userId, TerminalUtil.nextNumber()));
    }

    @Override
    public String name() {
        return "task-view-by-index";
    }

}
