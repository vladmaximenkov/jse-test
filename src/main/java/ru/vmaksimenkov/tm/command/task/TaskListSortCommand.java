package ru.vmaksimenkov.tm.command.task;

import ru.vmaksimenkov.tm.enumerated.Sort;
import ru.vmaksimenkov.tm.exception.entity.SortNotFoundException;
import ru.vmaksimenkov.tm.model.Task;
import ru.vmaksimenkov.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Stream;

public final class TaskListSortCommand extends AbstractTaskCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Show task list sorted";
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[TASK LIST SORTED]");
        System.out.println("ENTER SORT:");
        System.out.println(Arrays.toString(Sort.values()));
        Stream<Task> stream;
        try {
            stream = serviceLocator.getTaskService().findAll(userId, Sort.getSort(TerminalUtil.nextLine().toUpperCase()).getComparator());
        } catch (SortNotFoundException e) {
            System.err.println(e.getMessage() + " Default sort instead");
            stream = serviceLocator.getTaskService().findAll(userId);
        }
        System.out.printf("\t| %-36s | %-12s | %-20s | %-30s | %-30s | %-30s | %s %n", "ID", "STATUS", "NAME", "CREATED", "STARTED", "FINISHED", "PROJECT");
        AtomicInteger index = new AtomicInteger(1);
        stream.forEach((x) -> System.out.println(index.getAndIncrement() + "\t" + x));
    }

    @Override
    public String name() {
        return "task-list-sort";
    }

}
