package ru.vmaksimenkov.tm.exception;

public class AbstractException extends RuntimeException {

    public AbstractException(String s) {
        super(s);
    }

    public AbstractException(Throwable e) {
        super(e);
    }

}
