package ru.vmaksimenkov.tm.repository;

import ru.vmaksimenkov.tm.api.IRepository;
import ru.vmaksimenkov.tm.model.AbstractEntity;

import java.util.Comparator;
import java.util.Optional;
import java.util.function.Supplier;
import java.util.stream.Stream;

public abstract class AbstractRepository<E extends AbstractEntity> implements IRepository<E> {

    protected Stream<E> stream = Stream.empty();

    protected Supplier<Stream<E>> list = () -> stream;

    @Override
    public void add(E e) {
        stream = Stream.concat(list.get(), Stream.of(e));
    }

    @Override
    public void clear(final String userId) {
        stream = list.get().filter(e -> !userId.equals(e.getUserId()));
    }

    @Override
    public boolean existsById(final String userId, final String id) {
        return list.get().anyMatch(e -> userId.equals(e.getUserId()) && id.equals(e.getUserId()));
    }

    @Override
    public Stream<E> findAll(final String userId, final Comparator<E> comparator) {
        return list.get().filter(e -> userId.equals(e.getUserId())).sorted(comparator);
    }

    @Override
    public Stream<E> findAll(final String userId) {
        return list.get().filter(e -> userId.equals(e.getUserId()));
    }

    @Override
    public Optional<E> findById(final String userId, final String id) {
        return list.get().filter(e -> userId.equals(e.getUserId()) && id.equals(e.getId())).findAny();
    }

    @Override
    public void remove(final E e) {
        stream = list.get().filter(e1 -> !e1.equals(e));
    }

    @Override
    public void removeById(final String userId, final String id) {
        stream = list.get().filter(e -> !(userId.equals(e.getUserId()) && id.equals(e.getId())));
    }

    @Override
    public int size() {
        return (int) list.get().count();
    }

    @Override
    public int size(String userId) {
        return (int) list.get().filter(e -> userId.equals(e.getUserId())).count();
    }

}
