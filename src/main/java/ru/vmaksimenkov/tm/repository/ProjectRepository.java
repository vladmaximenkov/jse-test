package ru.vmaksimenkov.tm.repository;

import ru.vmaksimenkov.tm.api.repository.IProjectRepository;
import ru.vmaksimenkov.tm.exception.entity.ProjectNotFoundException;
import ru.vmaksimenkov.tm.exception.entity.TaskNotFoundException;
import ru.vmaksimenkov.tm.model.Project;
import ru.vmaksimenkov.tm.model.Task;

import java.util.Optional;

public class ProjectRepository extends AbstractRepository<Project> implements IProjectRepository {

    @Override
    public boolean existsByName(final String userId, final String name) {
        return list.get().anyMatch(e -> userId.equals(e.getUserId()) && name.equals(e.getName()));
    }

    @Override
    public Optional<Project> findOneByIndex(final String userId, final Integer index) {
        return list.get().filter(e -> userId.equals(e.getUserId())).sorted().skip(index - 1).findFirst();
    }

    @Override
    public Optional<Project> findOneByName(final String userId, final String name) {
        return list.get().filter(e -> userId.equals(e.getUserId()) && name.equals(e.getName())).findFirst();
    }

    @Override
    public String getIdByIndex(final String userId, final Integer index) {
        Optional<Project> project = list.get().filter(e -> userId.equals(e.getUserId())).skip(index - 1).findFirst();
        if (project.isPresent()) return project.get().getId();
        else throw new ProjectNotFoundException();
    }

    @Override
    public String getIdByName(final String userId, final String name) {
        Optional<Project> project = list.get().filter(e -> userId.equals(e.getUserId()) && name.equals(e.getName())).findFirst();
        if (project.isPresent()) return project.get().getId();
        else throw new ProjectNotFoundException();
    }

    @Override
    public void removeOneByIndex(final String userId, final Integer index) {
        Optional<Project> task = list.get().filter(e -> userId.equals(e.getUserId())).skip(index - 1).findFirst();
        if (task.isPresent()) {
            stream = list.get().filter(task.get()::equals);
        } else throw new TaskNotFoundException();
    }

    @Override
    public void removeOneByName(final String userId, final String name) {
        stream = list.get().filter(e -> !(userId.equals(e.getUserId()) && name.equals(e.getName())));
    }

}
