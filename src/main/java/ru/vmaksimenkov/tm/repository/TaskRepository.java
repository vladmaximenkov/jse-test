package ru.vmaksimenkov.tm.repository;

import ru.vmaksimenkov.tm.api.repository.ITaskRepository;
import ru.vmaksimenkov.tm.exception.entity.TaskNotFoundException;
import ru.vmaksimenkov.tm.model.Task;

import java.util.Optional;
import java.util.stream.Stream;

import static ru.vmaksimenkov.tm.util.ValidationUtil.isEmpty;

public class TaskRepository extends AbstractRepository<Task> implements ITaskRepository {

    @Override
    public void bindTaskPyProjectId(final String userId, final String projectId, final String taskId) {
        findById(userId, taskId).ifPresent(e -> e.setProjectId(projectId));
    }

    @Override
    public boolean existsByName(final String userId, final String name) {
        return list.get().anyMatch(e -> userId.equals(e.getUserId()) && name.equals(e.getName()));
    }

    @Override
    public boolean existsByProjectId(final String userId, final String projectId) {
        return list.get().anyMatch(e -> userId.equals(e.getUserId()) && projectId.equals(e.getProjectId()));
    }

    @Override
    public Stream<Task> findAllByProjectId(final String userId, final String projectId) {
        return list.get().filter(e -> userId.equals(e.getUserId()) && projectId.equals(e.getProjectId()));
    }

    @Override
    public Optional<Task> findOneByIndex(final String userId, final Integer index) {
        return list.get().filter(e -> userId.equals(e.getUserId())).sorted().skip(index - 1).findFirst();
    }

    @Override
    public Optional<Task> findOneByName(final String userId, final String name) {
        return list.get().filter(e -> userId.equals(e.getUserId()) && name.equals(e.getName())).findFirst();
    }

    @Override
    public String getIdByIndex(final String userId, final Integer index) {
        Optional<Task> task = list.get().filter(e -> userId.equals(e.getUserId())).skip(index - 1).findFirst();
        if (task.isPresent()) return task.get().getId();
        else throw new TaskNotFoundException();
    }

    @Override
    public void removeAllBinded(final String userId) {
        stream = list.get().filter(e -> !(userId.equals(e.getUserId()) && !isEmpty(e.getProjectId())));
    }

    @Override
    public void removeAllByProjectId(final String userId, final String projectId) {
        stream = list.get().filter(e -> !(userId.equals(e.getUserId()) && projectId.equals(e.getProjectId())));
    }

    @Override
    public void removeOneByIndex(final String userId, final Integer index) {
        Optional<Task> task = list.get().filter(e -> userId.equals(e.getUserId())).skip(index - 1).findFirst();
        if (task.isPresent()) {
            stream = list.get().filter(task.get()::equals);
        } else throw new TaskNotFoundException();
    }

    @Override
    public void removeOneByName(final String userId, final String name) {
        stream = list.get().filter(e -> !(userId.equals(e.getUserId()) && name.equals(e.getName())));
    }

    @Override
    public Optional<Task> unbindTaskFromProject(final String userId, final String taskId) {
        Optional<Task> task = list.get().filter(e -> userId.equals(e.getUserId())).findFirst();
        task.ifPresent(e -> e.setProjectId(null));
        return task;
    }

}
