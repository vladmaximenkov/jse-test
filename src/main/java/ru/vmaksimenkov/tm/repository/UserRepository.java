package ru.vmaksimenkov.tm.repository;

import ru.vmaksimenkov.tm.api.repository.IUserRepository;
import ru.vmaksimenkov.tm.exception.entity.UserNotFoundException;
import ru.vmaksimenkov.tm.model.User;
import ru.vmaksimenkov.tm.util.HashUtil;

import java.util.Optional;

public class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @Override
    public boolean existsByEmail(final String email) {
        return list.get().anyMatch(e -> email.equals(e.getEmail()));
    }

    @Override
    public boolean existsByLogin(final String login) {
        return list.get()
                .anyMatch((e) -> (
                        login.equals(e.getLogin())
                ));
    }

    @Override
    public Optional<User> findById(final String id) {
        return list.get().filter(e -> id.equals(e.getId())).findAny();
    }

    @Override
    public Optional<User> findByLogin(final String login) {
        return list.get().filter(e -> login.equals(e.getLogin())).findAny();
    }

    @Override
    public void removeByLogin(final String userId, final String login) {
        stream = list.get().filter(e -> !(userId.equals(e.getUserId()) && login.equals(e.getLogin())));
    }

    @Override
    public void setPasswordById(final String userId, final String id, final String password) {
        Optional<User> user = findById(userId, id);
        if (user.isPresent()) user.get().setPasswordHash(HashUtil.salt(password));
        else throw new UserNotFoundException();
    }

}
