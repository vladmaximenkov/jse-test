package ru.vmaksimenkov.tm.util;

public interface HashUtil {

    Integer ITERATION = 20000;
    String SECRET = "XwsSAdc6wZj2pSmi7";

    static String md5(final String s) {
        if (s == null) return null;
        try {
            java.security.MessageDigest md = java.security.MessageDigest.getInstance("MD5");
            final byte[] array = md.digest(s.getBytes());
            final StringBuilder sb = new StringBuilder();
            for (byte b : array) {
                sb.append(Integer.toHexString((b & 0xFF) | 0x100), 1, 3);
            }
            return sb.toString();
        } catch (java.security.NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }
    }

    static String salt(final String s) {
        if (s == null) return null;
        String result = s;
        for (int i = 0; i < ITERATION; i++) {
            result = md5(SECRET + result + SECRET);
        }
        return result;
    }

}